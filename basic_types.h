#ifndef BASIC_TYPES_H
#define BASIC_TYPES_H

// stl
#include <bitset>
#include <array>
#include <vector>
#include <set>
#include <unordered_set>
#include <utility>
#include <memory>
#include <chrono>
#include <ciso646>

namespace othello
{

  // 8x8 othello board
  namespace detail {
    constexpr size_t computeBoardSize() { return 8 * 8; }
    constexpr size_t invalidBoardPosition() { return computeBoardSize()+1; }
  }

  struct BitPos {
    constexpr BitPos() : m_bitpos{detail::invalidBoardPosition()} {}
    constexpr explicit BitPos(size_t bitpos) : m_bitpos{bitpos} {}
    constexpr explicit BitPos(int bitpos) : m_bitpos{size_t(bitpos)} {}

    BitPos(const BitPos&) = default;

    BitPos& operator=(const BitPos& o) {
      m_bitpos = o.m_bitpos;
      return *this;
    }

    static constexpr BitPos invalid()
    {
      return BitPos{detail::invalidBoardPosition()};
    }
    constexpr size_t value() const { return m_bitpos; }
    constexpr bool   isValid() const
    {
      return m_bitpos not_eq detail::invalidBoardPosition();
    }

    constexpr bool operator<(const BitPos& other) const
    {
      return m_bitpos < other.m_bitpos;
    }

    constexpr bool operator==(const BitPos& other) const
    {
      return m_bitpos == other.m_bitpos;
    }
    constexpr bool operator!=(const BitPos& other) const
    {
      return !(*this == other);
    }

  private:
    size_t m_bitpos;
  };

  enum class MoveDirection { N, S, E, W, NE, NW, SE, SW };

}   // END namespace othello

namespace std
{

  template <>
  struct hash<othello::BitPos> {
    size_t operator()(const othello::BitPos& bitpos) const
    {
      return std::hash<size_t>{}(bitpos.value());
    }
  };

}   // END namespace std


namespace othello   // resume namespace othello
{
  using  BitPieces = std::bitset<detail::computeBoardSize()>;
  using BitBoard  = std::array<BitPieces,2>;

  enum class PlayerId : size_t { One = 0, Two };
  enum class PlayerType { Human, AI };

  using PlayerIdSet = std::set<PlayerId>;

  using BitPosSet = std::unordered_set<BitPos>;

}   // END namespace othello

#endif   // BASIC_TYPES_H
