#ifndef ENGINE_INTERFACE_H
#define ENGINE_INTERFACE_H



#include "basic_types.h"
#include "player_interface.h"

// stl
#include <memory>
#include <iostream>


namespace othello {

  namespace detail {

    struct PlayerStruct {
      std::unique_ptr<PlayerInterface> obj;
      PlayerType                       type;
    };
  } // namespace othello::detail


  class GameEngineInterface {
  public:
    GameEngineInterface()          = default;
    virtual ~GameEngineInterface() = default;

    /*! Init a new game for the set players */
    virtual bool initNewGame() = 0;

    /*! End and reset the game object to a clean state */
    virtual void clearGame() = 0;


    template <typename Player_T, othello::PlayerId PlayerId_T, typename... PlayerArg_Ts>
    void initPlayerType(PlayerArg_Ts... player_args)
    {
      static_assert(
        std::is_base_of<HumanPlayer, Player_T>::value
          or std::is_base_of<AIInterface, Player_T>::value,
        "Player class must be of either HumanPlayer or AIInterface base.");

      const auto player_type = std::is_base_of<HumanPlayer, Player_T>::value ? PlayerType::Human : PlayerType::AI;

      if constexpr (PlayerId(PlayerId_T) == PlayerId::One) {
        std::cout << "Hello player one!" << std::endl;
        m_player_one.obj  = std::make_unique<Player_T>(PlayerId::One,player_args...);
        m_player_one.type = player_type;
      }
      else {
        std::cout << "Hello player two!" << std::endl;
        m_player_two.obj  = std::make_unique<Player_T>(PlayerId::Two,player_args...);
        m_player_two.type = player_type;
      }

    }

    /*! Perform a move for the current HUMAN player
     *  \param[in] move What move to perform
     *  \return Returns true if a move was successfully made */
    virtual bool performMoveForCurrentHuman(const BitPos& move) = 0;

    /*! Enqueue thinking for the current player
     *  \param[in] max_time The time awailable for thinking */
    virtual void think(const std::chrono::seconds& max_time) = 0;

    /*! Return the player id of the current player */
    virtual PlayerId currentPlayerId() const = 0;

    /*! Return the player type of the current player */
    virtual PlayerType currentPlayerType() const = 0;

    /*! Query the bitboard piece position set of a given player
     * \param[in] player_id The given player
     * \return The current bitboard piece position set of the player */
    virtual BitPieces pieces(const PlayerId& player_id) const = 0;

    /*! Query the complete bitboard piece set for the current game position. */
    virtual const BitBoard& board() const = 0;



  protected:
    BitBoard             m_board; /*< BitBoard holding player piece positions */
    detail::PlayerStruct m_player_one;
    detail::PlayerStruct m_player_two;
  };


}   // END namespace othello




#endif // ENGINE_INTERFACE_H
